# Dell M4700 Hackintosh

## My system specs:

- CPU: Intel Core i7 - 3720QM
- RAM: 16 GB (8GB x 2) 1600 MHz DDR3, recommended at least 8GB for best experiences.
- Storage: 120 GB Solid State SATA Drive (for system and applications must install in here only like Clover bootloader, Virtualbox, applications from App Store, Adobe applications, Microsoft Office, ...) + 640GB SATA Disk (after installation, Users folder will be moved here by me for separated Applications folder).

I pinned Applications folders on HDD to Favourites and install third party applications in here.

![Two Applications folders](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/first-applications-folder-is-on-640g-hdd.png)

Storage in About This Mac

![Storage in About This Mac](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/storage.png)

Storage in System Report

![Storage in System Report](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/storage-in-system-report.png)

- iGPU: Intel HD4000
- GPU: NVIDIA Quadro K1000M

![GPU](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/graphics-display-in-system-report.png)

- Screen: 1920x1080 60Hz
- Audio: IDT92HD93BXX
- Wifi: Replaced to BCM943224 HMS (No Bluetooth)
- Bluetooth: Built-in
- Camera: Built-in

## Patchs applied:

### DSDT/SSDT:
- 3 PARSEOP_* errors - [Thread link](https://www.tonymacx86.com/threads/solved-help-me-fix-dsdt-error.229025/)
- SSDT-PNLF.aml 

### Clover hotpatch explanations

#### Graphics
- ig-flatform-id: 0x01660004 (HD4000) and Dual Link = 1
- Inject Intel only
- Nvidia not supported in Optimus configuration, so injecting Nvidia not helpful ([Source](https://www.tonymacx86.com/threads/installing-on-dell-precision-m4700-bunch-of-issues.208314/#post-1383146))

#### Kernel and kext patchs:
- USB 10.13.4+ by PMHeart

### Kexts:
- Enable wifi: Lilu + AirportBrcmFixup
- Enable Bluetooth + Camera: USBInjectAll
- Enable audio: Lilu + AppleALC + inject layout-id 12 in config.plist / Devices
- All kexts should be updated to the lastest version to work perfectly.

## Patchs not applied:
- Sleep when close the lid.
- FaceTime + iMessages.
- More if you find out ...

![About](https://gitlab.com/4fingers-hackintosh/dell-m4700/raw/master/screenshots/about.png)

